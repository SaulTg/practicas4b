package com.example.usuario.primeraapp4b;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Activity_ActividadPrincipal extends AppCompatActivity {
Button buttonLogin, buttonBuscar, buttonGuardar, buttonParametros, buttonFragmentos, buttonAutenticar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__actividad_principal);
        buttonLogin = findViewById(R.id.buttonLOGIN);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        Activity_ActividadPrincipal.this, ActividadLogin.class);
                startActivity(intent);
            }
        });
        buttonBuscar = findViewById(R.id.buttonBUSCAR);
        buttonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        Activity_ActividadPrincipal.this, ActivididadBuscar.class);
                startActivity(intent);
            }
        });
        buttonGuardar = findViewById(R.id.buttonGUARDAR);
        buttonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        Activity_ActividadPrincipal.this, ActividadRegistrar.class);
                startActivity(intent);
            }
        });
        buttonParametros = findViewById(R.id.buttonParametros);
        buttonParametros.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                    Activity_ActividadPrincipal.this, ActividadPasarParametros.class);
                startActivity(intent);
            }
        });
        buttonFragmentos = findViewById(R.id.buttonFRAGMENTOS);
        buttonFragmentos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        Activity_ActividadPrincipal.this, ActividadFragmentos.class);
                startActivity(intent);
            }
        });

    }
    public boolean onCreateOptionsMenu (Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.opcionLogin:
                intent = new Intent(Activity_ActividadPrincipal.this, ActividadLogin.class);
                startActivity(intent);
                break;
            case R.id.opcionRegistrar:
                intent = new Intent(Activity_ActividadPrincipal.this, ActividadRegistrar.class);
                startActivity(intent);
                break;
            case R.id.opcionBuscar:
                intent = new Intent(Activity_ActividadPrincipal.this, ActivididadBuscar.class);
                startActivity(intent);
                break;
            case R.id.opcionPasarParametros:
                intent = new Intent(Activity_ActividadPrincipal.this, ActividadPasarParametros.class);
                startActivity(intent);
                break;
            case R.id.opcionLOGIN:
                Dialog dialogoLogin = new Dialog(Activity_ActividadPrincipal.this);
                dialogoLogin.setContentView(R.layout.dlg_login);

                Button btnautenticar = (Button) dialogoLogin.findViewById(R.id.buttonAutenticar);
                final EditText Cajauser = (EditText) dialogoLogin.findViewById(R.id.txtUser);
                final EditText Cajaclave = (EditText) dialogoLogin.findViewById(R.id.txtContraseña);

                btnautenticar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(Activity_ActividadPrincipal.this,"Usuario: "+ Cajauser.getText().toString()+"  Clave: "+Cajaclave.getText().toString(),Toast.LENGTH_LONG).show();
                    }
                });
                dialogoLogin.show();
                break;
            case R.id.opcionREGISTRAR:
                Dialog dialogoRegistrar = new Dialog(Activity_ActividadPrincipal.this);
                dialogoRegistrar.setContentView(R.layout.dlg_registrar);
                Button btnregistrar=(Button)dialogoRegistrar.findViewById(R.id.buttonRegistrar);
                final EditText cajanombre=(EditText)dialogoRegistrar.findViewById(R.id.txtNombres);
                final EditText cajapellido=(EditText)dialogoRegistrar.findViewById(R.id.txtApellidos);

                btnregistrar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(Activity_ActividadPrincipal.this, "Nombres: " + cajanombre.getText().toString() + "  Apellidos: " + cajapellido.getText().toString(), Toast.LENGTH_LONG).show();
                    }
                });
                dialogoRegistrar.show();
                break;


        }
        return true;
    }
}
